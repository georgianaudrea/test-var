const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.cars = [
    {
        make: "BMW",
        model: "X6",
        price: 50000
    },
    {
        make: "Lamborghini",
        model: "Huracan",
        price: 200000
    },
];

const isEmpty = obj => {
    for(let key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

app.get('/cars', (req, res) => {
    res.status(200).json(app.locals.cars);
});

app.post('/cars', (req, res, next) => {
     if (isEmpty(req.body)) return res.status(500).send({message: "Body is missing"});
    if (!(
        req.body.hasOwnProperty('make') && 
        req.body.hasOwnProperty('model') && 
        req.body.hasOwnProperty('price')
        )) {
            return res.status(500).send({message: "Invalid body format"});
        }
    if (req.body.price <= 0) return res.status(500).send({message: "Price should be a positive number"});
    const index = app.locals.cars.findIndex(item => item.model === req.body.model);
    if (index > -1) return res.status(500).send({message: "Car already exists"});
    app.locals.cars.push(req.body);
    return res.status(201).json({message: "Created"});
})

module.exports = app;