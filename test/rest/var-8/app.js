const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.students = [
    {
        name: "Gigel",
        surname: "Popel",
        age: 23
    },
    {
        name: "Gigescu",
        surname: "Ionel",
        age: 25
    }
];

app.get('/students', (req, res) => {
    res.status(200).json(app.locals.students);
});

const isEmpty = obj => {
    for(let key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

app.post('/students', (req, res, next) => {
    if (isEmpty(req.body)) return res.status(500).send({message: "Body is missing"});
    if (!(
        req.body.hasOwnProperty('name') && 
        req.body.hasOwnProperty('surname') && 
        req.body.hasOwnProperty('age')
        )) {
            return res.status(500).send({message: "Invalid body format"});
        }
    if (req.body.age <= 0) return res.status(500).send({message: "Age should be a positive number"});
    const index = app.locals.students.findIndex(item => item.name === req.body.name);
    if (index > -1) return res.status(500).send({message: "Student already exists"});
    app.locals.students.push(req.body);
    return res.status(201).json({message: "Created"});
})

module.exports = app;