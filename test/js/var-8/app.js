function addTokens(input, tokens){
    try {
         if (typeof input !== 'string') throw new Error('Invalid input');
        if (input.length < 6) throw new Error('Input should have at least 6 characters');
        if (!Array.isArray(tokens)) throw new Error('Invalid array format');
        for (let i = 0; i < tokens.length; i++) {
            if (!tokens[i].hasOwnProperty('tokenName') || typeof tokens[i].tokenName !== 'string') {
                throw new Error('Invalid array format');
            }
        }
        
        const splitInput = input.split('...');
        let j = 0;
        for(let i = 0;i < splitInput.length;i++) {
            if (splitInput[i] === '') {
                splitInput[i] = '${' + tokens[j].tokenName + '}';
                j++;
            }
        }
        
        return splitInput.join('');
    } catch (e) {
        throw e;
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;