import React from 'react';

import { AddEmployee } from './AddEmployee';

export class EmployeeList extends React.Component {
    constructor(){
        super();
        this.state = {
            employees: []
        };
    }
    
    onAddEmployee = (employee) => {
        this.setState(prevState => ({
            employees: [...prevState.employees, employee]
        }));
    }

    render(){
        const employeeList = this.state.employees.length ? this.state.employees.map(item => (
            <div key={item.name}>
                {item.name}/{item.surname}/{item.experience}
            </div>
            )) : null;
        return(
            <div>
                 {employeeList}
                <AddEmployee onAdd={this.onAddEmployee} />
            </div>
        )
    }
}