import React from 'react';

export class AddEmployee extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            surname: '',
            experience: ''
        };
    }

    addEmployee = () => {
        let employee = {
            name: this.state.name,
            surname: this.state.surname,
            experience: this.state.experience
        };
        this.props.onAdd(employee);
    }

    render(){
        return (
            <div>
                <input id="name" type="text" name="name" value={this.state.name} onChange={e => this.setState({ name: e.target.value }) } />
                <input id="surname" type="text" name="surname" value={this.state.surname} onChange={e => this.setState({ surname: e.target.value }) } />
                <input id="experience" type="text" name="experience" value={this.state.experience} onChange={e => this.setState({ experience: e.target.value }) } />
                <input type="button" value="add employee" onClick={this.addEmployee} />
            </div>
        )
    }
}