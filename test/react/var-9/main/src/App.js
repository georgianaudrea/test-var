import React, { Component } from 'react';

import { CouponList } from './CouponList'

class App extends Component {
  render() {
    return (
      <div>
        <CouponList />
      </div>
    );
  }
}

export default App;
