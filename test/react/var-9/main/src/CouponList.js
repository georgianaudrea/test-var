import React from 'react';

import { AddCoupon } from './AddCoupon';

export class CouponList extends React.Component {
    constructor(){
        super();
        this.state = {
            coupons: []
        };
    }
    
    onAddCoupon = (coupon) => {
        this.setState(prevState => ({
            coupons: [...prevState.coupons, coupon]
        }));
    }

    render(){
        const couponList = this.state.coupons.length ? this.state.coupons.map(item => (
            <div key={item.category}>
                {item.category}/{item.discount}/{item.availability}
            </div>
            )) : null;
        return(
            <div>
                {couponList}
                <AddCoupon onAdd={this.onAddCoupon} />
            </div>
        )
    }
}