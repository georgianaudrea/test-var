import React from 'react';

export class AddCoupon extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            category: '',
            discount: '',
            availability: ''
        };
    }

    addCoupon = () => {
        let coupon = {
            category: this.state.category,
            discount: this.state.discount,
            availability: this.state.availability
        };
        this.props.onAdd(coupon);
    }

    render(){
        return(
            <div>
                <input id="category" type="text" name="category" value={this.state.category} onChange={e => this.setState({ category: e.target.value }) } />
                <input id="discount" type="text" name="discount" value={this.state.discount} onChange={e => this.setState({ discount: e.target.value }) } />
                <input id="availability" type="text" name="availability" value={this.state.availability} onChange={e => this.setState({ availability: e.target.value }) } />
                <input type="button" value="add coupon" onClick={this.addCoupon} />
            </div>
        )
    }
}